#!/bin/bash
set -e

file=$1

checkUtils() {
    # Check CURL
    if ! [ -x $(command -v curl ) ]; then
        echo "Error: curl is not installed." >&2
        exit 127
    fi

    # Check dig
    if ! [ -x $(command -v dig ) ]; then
        echo "Error: dig is not installed." >&2
        exit 127
    fi     
}

checkFile() {
    if [ -s $file ]; then
        checkUrls $file
    else 
        echo " file $file does not exist, or is empty " >&2
        exit 1
    fi

}


checkUrls() {
    input=$1
    while IFS=' ' read -r line 
    do
        ip=`echo $line | sed -e 's|^[^/]*//||' -e 's|/.*$||' | xargs dig +short `
        if [[ -n $ip ]]; then 
            #siteAnswer=$(curl -IL -s $line | head -n 1 | cut -d$' ' -f2)
            siteAnswer=$(curl -IL -o /dev/null -w "%{http_code}" --silent $line )
            if  [[ $siteAnswer =~ ^(1[0-9][0-9]|2[0-9][0-9]|3[0-9][0-9])$  ]]; then
                echo "$line is okay; its answer is $siteAnswer"
            else
                echo -e "$line not okay; its answer is $siteAnswer \nother skipped" >&2
                exit 1        
            fi
        else
            echo Could not resolve hostname $line.
            exit 6
        fi
    done < "$input"

}


checkUtils
checkFile